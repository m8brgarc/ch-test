// Initialize modules
// Importing specific gulp API functions lets us write them below as series() instead of gulp.series()
const { src, dest, watch, series, parallel } = require('gulp');
// Importing all the Gulp-related packages we want to use
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const rename = require('gulp-rename');
const webserver = require('gulp-webserver');
const babel = require('gulp-babel');
let replace = require('gulp-replace');


// File paths
const files = {
    scssPath: 'src/scss/**/*.scss',
    jsPath: 'src/js/**/*.js'
};

// Sass task: compiles the style.scss file into style.css
function scssTask(){
    return src([
        'node_modules/normalize.css/normalize.css',
        files.scssPath
    ])
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(postcss([ autoprefixer(), cssnano() ]))
        .pipe(concat('all.css'))
        .pipe(sourcemaps.write('.'))
        .pipe(rename('bundle.min.css'))
        .pipe(dest('assets/css')
        );
}

// JS task: concatenates and uglifies JS files to script.js
function jsTask(){
    return src([
        'node_modules/babel-polyfill/dist/polyfill.js',
        files.jsPath
        //,'!' + 'includes/js/jquery.min.js', // to exclude any specific files
    ])
        .pipe(babel({presets: ['@babel/preset-env']}))
        .pipe(concat('all.js'))
        // .pipe(uglify())
        .pipe(rename('bundle.min.js'))
        .pipe(dest('assets/js')
        );
}

// Cachebust
let cbString = new Date().getTime();
function cacheBustTask(){
    return src(['index.html'])
        .pipe(replace(/cb=\d+/g, 'cb=' + cbString))
        .pipe(dest('.'));
}

// Watch
function watchTask(){
    watch([files.scssPath], scssTask);
    watch([files.jsPath], jsTask);
}

// Serverf
function serverTask() {
    return src('.')
        .pipe(webserver({
            livereload: true,
            open: true,
            port: 8080
        }));
}

// Export tasks
exports.serve = serverTask;
exports.watch = watchTask;
exports.compile = parallel(scssTask, jsTask);
exports.default = series(
    parallel(scssTask, jsTask),
    cacheBustTask,
    serverTask,
    watchTask
);