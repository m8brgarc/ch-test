const ready = (func) => {
    if(document.attachEvent ? document.readyState === 'complete' : document.readyState !== 'loading') {
        func();
    } else {
        document.addEventListener('DOMContentLoaded', func);
    }
};

ready(() => {
    // Flatpickr (date picker)
    flatpickr('#date-start', {
        onChange: (selectedDates, dateStr, instance) => {
            console.log(instance);
        }
    });

    flatpickr('#date-end', {
        onChange: (selectedDates, dateStr, instance) => {

        }
    });

    // Truncate Text
    function truncateText(text, len = 50) {
        return text.length > len ? text.substring(0,len) + '...' : text;
    }

    // Switch Image
    function switchImage(img) {
        if(img.src.indexOf('_Color') > -1) {
            img.src = img.src.replace('_Color.png', '.png');
        } else {
            img.src = img.src.replace('.png', '_Color.png');
        }
    }

    // Generate Cards
    const generateCards = (apiData) => {
        let cont = document.getElementById('container'),
            content = '';

        apiData.content.forEach(item => {
            content += `<figure data-id="${item.content_id}">`;
            content += `<h4 title="${item.content_name}">${truncateText(item.content_name, 30)}</h4>`;
            content += `<div class="tags">`;

            // Get Tags
            item.tags.forEach((tag, idx) => {
                content += `<span data-id="${tag.tag_id}">${tag.tag_name}</span>${idx !== item.tags.length - 1 ? ', ' : ''}`
            });

            content += '</div>';
            content += '<img src="http://placehold.it/480x260?text=No+Image" alt="No Image">';
            content += '<figcaption>';
            content += `<p>${truncateText(item.content_social_description)}</p>`;
            content += '<footer>';
            content += `<span class="date">${item.content_date_literal_range}</span>`;
            content += `<a href="#${item.content_slug}"><span><img src="/assets/images/NavigationIcon_Volunteering_Color.png" width="20" alt="Learn More"></span> <span>Learn More</span></a>`;
            content += '</footer>';
            content += '</figcaption>';
            content += '</figure>';
        });

        cont.innerHTML = content;
    };

    // Get API
    const getApiData = url => {
        let apiReq = new XMLHttpRequest();
        apiReq.open('GET', url);
        apiReq.onload = () => {
            if(apiReq.status >= 200 && apiReq.status < 400) {
                let resp = JSON.parse(apiReq.responseText);
                console.log(resp);

                generateCards(resp);
            } else {
                console.log(apiReq);
                return null;
            }
        };
        apiReq.onerror = err => {
            console.log(err)
        };
        apiReq.send();
    };

    getApiData('http://hardingdevelopment.nexisit.net/harding_api/api_event_search.php?page_num=0&per_page=20&buckets=Volunteering&tag=26&timezone=25200&app_server_version=3.2&app_version=2&app_build=1&user_id=2&token=70aedda35dca9c192ef551c9f7b570e0&salt=309a9bea4d2695656e83f4fe7b340ee0&app=1&version=3.2');


    // Generate nav
    const navData = [
        {url: '/home', text: 'Home'},
        {url: '/stories', text: 'Stories'},
        {url: '/volunteering', text: 'Volunteering'}
    ],
        mobileNav = document.getElementById('mobile-menu'),
        footNav = document.getElementById('footer-nav');

    // Generate Link for nav
    const makeLink = (item, hasImg = false) => {
        let li = document.createElement('li'),
            img = (hasImg) ? `<img src="/assets/images/NavigationIcon_${item.text}.png" width="40" alt="${item.text}">` : '';

        li.innerHTML = `<a class="${hasImg ? 'switcher' : ''}" href="${item.url}">${img}${item.text}</a>`;
        return li;
    };
    const makeNav = data => {
        let mUl = document.createElement('ul'),
            fUl = mUl.cloneNode(true);

        mUl.classList = fUl.classList = 'unstyled';

        data.forEach(item => {
            mUl.appendChild(makeLink(item));
            fUl.appendChild(makeLink(item, true));
        });

        mobileNav.appendChild(mUl);
        footNav.appendChild(fUl);
    };
    makeNav(navData);

    // Events
    const menuBtn = document.getElementById('menu-btn');
    menuBtn.addEventListener('click', () => mobileNav.classList.toggle('open'));

    const navSwitcher = document.getElementsByClassName('switcher');
    for(let i = 0; i < navSwitcher.length; i++) {
        navSwitcher[i].addEventListener('click', (e) => {
            e.preventDefault();
            e.target.classList.toggle('color-primary');
            switchImage(e.target.childNodes[0]);
        });
    }

    const filterBtn = document.getElementById('filter-btn'),
        filterForm = document.getElementById('filter-form');
    filterBtn.addEventListener('click', () => filterForm.classList.toggle('open'));
});